package cat.boscdelacoma.uf5.cafeteria;

/**
 * Exemple on se simula una cafeteria on ser serveix una tassa de cafè a un client
 * 
 * @author Marc Nicolau
 */
public class Cafeteria {

    public static void main(String[] args) {
        TassaCafe tassa = new TassaCafe(98);
        Client client = new Client();

        servirClientV1(client, tassa);
        servirClientV2(client, tassa);
    }
    /**
     * En aquest subprograma es gestiona l'excepció de manera particular.
     * @param client el client a qui se serveix el cafè
     * @param cafe la tassa de cafè
     */
    static void servirClientV1(Client client, TassaCafe cafe) {
        try {
            client.beureCafe(cafe);
            System.out.println("El cafè està perfecte!");
        } catch (MassaFredException e) {
            System.out.println("Massa fred!");
        } catch (MassaCalentException e) {
            System.out.println("Massa calent!");
        }
    }

    /**
     * En aquest subprograma es gestiona l'excepció de manera global.
     * @param client el client a qui se serveix el cafè
     * @param cafe la tassa de cafè
     */
    static void servirClientV2(Client client, TassaCafe cafe) {
        try {
            client.beureCafe(cafe);
            System.out.println("El cafè està perfecte!");
        } catch (TemperaturaException e) {
            System.out.println("Massa fred o massa calent");
        }
    }
}
