package cat.boscdelacoma.uf5.cafeteria;

/**
 * Classe que modela una tassa de cafè.
 * 
 * 
 * @author Marc Nicolau
 */
class TassaCafe {
    /**
     * Temperatura considerada com a perfecta per a un cafè.
     */
    private static final int TEMPERATURA_OK = 70;

    /**
     * guarda la temperatura del cafè.
     */
    private int temperatura;

    /** 
     * Constructor per defecte
     */
    public TassaCafe() {
        this.temperatura = TEMPERATURA_OK;
    }

    /**
     * Constructor amb paràmetres
     * 
     * @param temperatura la temperatura del cafè
     */
    public TassaCafe(int temperatura) {
        this.temperatura = temperatura;
    }
    
    /** 
     * obtenir la temperatura del cafè
     * 
     * @return la temperatura del cafè 
     */
    public int getTemperatura() {
        return temperatura;
    }

    /**
     * assignar la temperatura del cafè
     * 
     * @param temperatura la temperatura del cafè
     */
    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }
}
