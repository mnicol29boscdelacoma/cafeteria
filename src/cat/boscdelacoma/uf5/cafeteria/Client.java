package cat.boscdelacoma.uf5.cafeteria;

/**
 * Classe que modela un client d'una cafeteria
 *      El client té dos llindars de temperatura: un per massa fred i un altre
 *      per massa calent.
 * 
 * @author Marc Nicolau
 */
class Client {
    /**
     * constants que indiquen els valors dels dos llindars: massa fred / massa calent.
     */
    private static final int MASSA_FRED = 60;
    private static final int MASSA_CALENT = 80;
    
    /**
     * Mètode que controla si el cafè és massa fred o massa claent.
     * 
     * @param tassa l'objecte que conté el cafè.
     * @throws MassaFredException es produeix quan el cafè és massa fred
     * @throws MassaCalentException es produeix quan el cafè és massa calent
     */
    public void beureCafe(TassaCafe tassa) throws MassaFredException, MassaCalentException{
        if(tassa.getTemperatura() < MASSA_FRED) {
            throw new MassaFredException();
        } else if(tassa.getTemperatura() > MASSA_CALENT) {
            throw new MassaCalentException();
        }
    }
}
