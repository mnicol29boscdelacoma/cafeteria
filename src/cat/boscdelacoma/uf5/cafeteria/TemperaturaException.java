package cat.boscdelacoma.uf5.cafeteria;

/**
 * Classe que modela una excepció de tipus temperatura
 * 
 * @author Marc Nicolau
 */
public class TemperaturaException extends Exception {

    public TemperaturaException() {
        super();
    }
}
