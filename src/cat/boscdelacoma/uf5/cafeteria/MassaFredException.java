package cat.boscdelacoma.uf5.cafeteria;

/**
 * Classe que modela una excepció de temperatura massa freda
 * 
 * @author Marc Nicolau
 */
public class MassaFredException extends TemperaturaException {

    public MassaFredException() {
        super();
    }
}
