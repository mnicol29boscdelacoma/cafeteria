package cat.boscdelacoma.uf5.cafeteria;

/**
 * Classe que modela una excepció de temperatura massa calenta
 * 
 * @author Marc Nicolau
 */
public class MassaCalentException extends TemperaturaException {

    public MassaCalentException() {
        super();
    }
}
